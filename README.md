#Yet Another Init (YAI)

####What is Yet Another Init(YAI)?  
Well it's designed to be a very simplistic init system written in Python. It's not meant to do much though. 

####Installation  
Put the init-config.py (renamed to config), and the binaries for the restart/shutdown/halt functionalities in the /etc/yai folder. (NOTE: You need to get the files from their repos)

####Requirements  
It requires the following:  
- Python 3 (Python 3.4 is recomended)  
- Something like daemontools or daemontools-encore.  

##PFAQ (Possibly Frequently Asked Questions)

####Why make another init?  
Because of three reasons:  
1) I wanted to try to make an init  
2) For research purposes  
3) Because I wanted to make an init that I does the following:  
- Only inits the system.  
- Does very little in PID1 to lower the chance of crashing.  

####Why Python?  
Because I felt like it would be interesting to do. If anything it's a nice research oppertunity.   

####Is this ready?  
Not by a long shot, in fact it probably will never be 'ready'.

####Is this worth it?/Should I use this?  
Probably not. I highly doubt a Python init would be worth it, let alone my init.

####What do you think is the purpose of an init?  
Personally, it's to bring up the system and then hand off control to another program, such as daemontools-encore. I personally don't think that complex things should be done in the init, and as such should be done in a different process

####What happened to init-config and the c files?  
They were split into a seperate project. If you need/want them you can get them from their repos:
- [Yet Another Init Config](https://bitbucket.com/Chanku/yet-another-init-config)  
- [Shutdown-Stuff](https://bitbucket.com/Chanku/shutdown-stuff)

####Why is installer.py removed?  
Because of the removal of YAIC and SS it was outdated and I didn't feel like updating it. 

####I want to make my own init? What should I do?  
Well you should probably read [The Manjaro Experiments](www.troubleshooters.com/linux/init/manjaro_experiments.htm) (more specifically the section on [getting Epoch init running](www.troubleshooters.com/linux/init/manjaro_experiments.htm#getting_epoch_running)) by Steve Litt and [Demystifying the init system (PID 1)](https://felipec.wordpress.com/2013/11/04/init/) by Felipe Contreras. Both should provide a good amount of documentation with The Manjaro Experiments providing you with the knowledge to actually be able to install inits yourself. For further reading I suggest reading [Init System Features and Benefits](http://www.troubleshooters.com/linux/init/features_and_benefits.htm) where Steve Litt goes overthe features and benifits of different init systems, which provide a better insight into different init feautres. 
